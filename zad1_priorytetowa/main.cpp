#include<iostream>
#include <string>
#include "kolejka.hh"

using namespace std;

int dl_kolejki=0;

int main(int argc, char *argv[]){
  KolejkaPrio<char> kolejka;                        // kolejka priorytetowa
  kolejka.dodaj('a',9);
  kolejka.dodaj('b',9);
  kolejka.dodaj('c',8);
  kolejka.dodaj('d',7);
  kolejka.dodaj('e',6);
  kolejka.dodaj('f',6);
  kolejka.dodaj('g',5);
  kolejka.dodaj('h',4);
  kolejka.dodaj('i',2);
  kolejka.dodaj('j',3);
  kolejka.dodaj('k',1);
  cout<<endl;
  kolejka.screen();

  cout<<endl<<endl<<"Usuwane elementy wedlog priorytetu"<<endl<<endl;
  while(!kolejka.isEmpty()){
    try{
      cout<<kolejka.usunMin()<<" ";
    }catch(KolejkaPrioEmptyException){
    cout<<"Kolejka jest pusta"<<endl;
    }
  }
  cout<<endl<<endl;
  if(!kolejka.clear())cout<<"Zajęta pamiec: "<<kolejka.rozmiar()<<endl;  // jezeli nie cala pamiec zostala zwolniona to program poinformuje urzytkownika
  return 0;
}
