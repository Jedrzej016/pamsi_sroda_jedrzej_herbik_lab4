#include<iostream>
#include <string>
#include "Tree.hh"

using namespace std;

int main(int argc, char *argv[]){
  BinaryTree<int> drzewo;
  cout<<endl;
  for(int i=0;i<10;i++)drzewo.dodaj(i%5);  
  cout<<"Zawartosc korzenia drzewa binarnego: "<<drzewo.getRoot()<<endl;
  cout<<endl<<"Wyskokosc drzewa binarnego: "<<drzewo.wysokosc()<<endl<<endl;  
  while(!drzewo.isEmpty()){
    try{
      cout<<"Usunieto: "<<drzewo.usun()<<endl;
    }catch(TreeEmptyException){
      cout<<"Drzewo jest Puste"<<endl;
    }
  }
  drzewo.rozmiar();
  // cout<<"Is empty: "<<drzewo.isEmpty()<<endl;
  if(!drzewo.clear())cout<<"Nie zwolniono calej pamieci"<<endl;
  cout<<endl;
  return 0;
}
