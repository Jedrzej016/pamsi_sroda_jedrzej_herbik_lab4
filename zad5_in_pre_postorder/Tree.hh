#include<cstdlib>
#include<ctime>

#define WEZLY 2 // ilosc galezi z wezla

class TreeEmptyException{};
extern int dl_kolejki; // zmienna do okreslania przecieku pamieci

template <class Object>
class wezelDrzewa{
protected:
  Object zawartosc;    // dane
  wezelDrzewa<Object> *rodzic; // wskaznik do przodka
  wezelDrzewa<Object> *syn[WEZLY]; // wskazniki na galezie
public:
  wezelDrzewa(){rodzic=NULL;for(int i=0;i<WEZLY;i++)syn[i]=NULL;}
  Object& getZawartosc(){return zawartosc;}
  Object& setZawartosc(const Object &dane){zawartosc=dane;return zawartosc;}
  wezelDrzewa<Object>*& getSon(int i){return syn[i];}
  wezelDrzewa<Object>*& setSon(wezelDrzewa<Object> (*dane),int i){syn[i]=dane;return syn[i];}
  wezelDrzewa<Object>*& getParent(){return rodzic;}
  wezelDrzewa<Object>*& setParent(wezelDrzewa<Object> (*dane)){rodzic=dane;return rodzic;}
};

template <class Object>
class Tree{
protected:
  Object zawartosc;    // dane
  wezelDrzewa<Object> *root; // wskaznik do przodu  
  int dl;
  int pokolenie;
  int galaz;
  void preorder(wezelDrzewa<Object> *wezel);
  void postorder(wezelDrzewa<Object> *wezel);
  void inorder(wezelDrzewa<Object> *wezel);
  void clear(wezelDrzewa<Object> *wezel);
  int max_galaz(int liczba);
  wezelDrzewa<Object>* przejdz(wezelDrzewa<Object> *wezel,int wartosc);
  bool allIsNull(wezelDrzewa<Object> *wezel);
  int IsNull(wezelDrzewa<Object> *wezel);
public:
  Tree();
  ~Tree(void){clear(root);}
  int rozmiar() const;
  bool isEmpty() const; 
  bool clear();         // wyczysc
  Object& getRoot()throw(TreeEmptyException);
  Object& dodaj(const Object& obj);
  Object& usun()throw(TreeEmptyException);
  int& wysokosc();
  void preorder();
  void postorder();
  void inorder();
};

template <class Object>
Tree<Object>::Tree(){
  root=NULL;
  dl=0;
  pokolenie=0;
  galaz=0;
}

template <class Object>  // zwraca zawartosc korzenia
Object& Tree<Object>::getRoot()throw(TreeEmptyException){
  if(root!=NULL)return root->getZawartosc();
  throw TreeEmptyException();
}

template <class Object>  // wywolanie preorder
void Tree<Object>::preorder(){
  std::cout<<"Przejscie Preorder: "<<std::endl;;
  preorder(root);
  std::cout<<std::endl;
}

template <class Object>  // rekurencyjne preorder
void Tree<Object>::preorder(wezelDrzewa<Object> *wezel){
  if(wezel!=NULL){
    std::cout<<wezel->getZawartosc()<<" ";
    for(int i=0;i<WEZLY;i++)preorder(wezel->getSon(i));
  }
}

template <class Object>  // wywolanie postorder
void Tree<Object>::postorder(){
  std::cout<<"Przejscie Postorder: "<<std::endl;;
  postorder(root);
  std::cout<<std::endl;
}

template <class Object>  // rekurencyjne postorder
void Tree<Object>::postorder(wezelDrzewa<Object> *wezel){
  if(wezel!=NULL){
    for(int i=0;i<WEZLY;i++)postorder(wezel->getSon(i));
    std::cout<<wezel->getZawartosc()<<" ";
  }
}

template <class Object>  // wywolanie inorder
void Tree<Object>::inorder(){
  std::cout<<"Przejscie Inorder: "<<std::endl;;
  inorder(root);
  std::cout<<std::endl;
}

template <class Object>  // rekurencyjne inorder
void Tree<Object>::inorder(wezelDrzewa<Object> *wezel){
  if(wezel!=NULL){
    for(int i=0;i<WEZLY;i++){
      inorder(wezel->getSon(i));
      if(i==0)std::cout<<wezel->getZawartosc()<<" ";
    }
  }
}

template <class Object>  // przypisuje galaz wartosc jezeli jest wieksza od aktualnej
int Tree<Object>::max_galaz(int liczba){
  if(liczba>galaz)galaz=liczba;
  return liczba;
}

template <class Object>  // zwraca galaz czyli wysokosc drzewa
int& Tree<Object>::wysokosc(){
  przejdz(root,-1);// -1 jezeli mierzymy ilosc polaczen, 0 jezeli ilosc wezlow
  return galaz;
}

template <class Object>  // przechodzi cale drzewo by znalezc wysokosc drzewa
wezelDrzewa<Object>* Tree<Object>::przejdz(wezelDrzewa<Object> *wezel,int wartosc){
  if(wezel!=NULL){
    wartosc++;
    for(int i=0;i<WEZLY;i++)przejdz(wezel->getSon(i),max_galaz(wartosc));
  }
  return wezel;
}

template <class Object>  // zwraca ilosc elementow
int Tree<Object>::rozmiar() const{
  return dl;
}

template <class Object>
bool Tree<Object>::isEmpty() const{
  if(dl||root) return false;
  return true;
}

template <class Object>  // wywolanie czyszczenia
bool Tree<Object>::clear(){
  clear(root);
  galaz=0;
  if((root!=NULL)||dl)return false;
  return true;
}

template <class Object>  // rekurencyjne czyszczenie
void Tree<Object>::clear(wezelDrzewa<Object> *wezel){
  if(wezel!=NULL){
    for(int i=0;i<WEZLY;i++)clear(wezel->getSon(i));
    delete wezel;
    if((wezel==root)&&(dl==1))root=NULL;
    dl--;
  }
}

template <class Object>                         // funkcja dodawania ktorza zaslonimy w kolejnej klasie
Object& Tree<Object>::dodaj(const Object& obj){
  wezelDrzewa<Object> *wezel=NULL;
  wezelDrzewa<Object> *tmp_back=NULL;
  wezelDrzewa<Object> *tmp=root;
  int wartosc=0;
  dl++;
  wezel=new wezelDrzewa<Object>;
  wezel->setZawartosc(obj);
  zawartosc=wezel->getZawartosc();
  while(tmp!=NULL){
    tmp_back=tmp;
    wartosc=std::rand()%WEZLY;
    tmp=tmp->getSon(wartosc);
  }
  wezel->setParent(tmp_back);
  if(tmp_back==NULL){
    root=wezel;
  }else{
    tmp_back->getSon(wartosc)=wezel;
  }
  return zawartosc;
}

template <class Object>
bool Tree<Object>::allIsNull(wezelDrzewa<Object> *wezel){  // zwraca true jezeli wszyscy synowie sa NULL
  if(wezel!=NULL)for(int i=0;i<WEZLY;i++)if(wezel->getSon(i)!=NULL)return 0;
  return 1;
}

template <class Object>  // zwraca liczbe syna ktory nie jest NULL liczac od lewej do konca
int Tree<Object>::IsNull(wezelDrzewa<Object> *wezel){
  int zwrot=0;
  if(wezel!=NULL)for(int i=0;i<WEZLY;i++)if(wezel->getSon(i)!=NULL)zwrot=i;
  return zwrot;
}

template <class Object>  // usuwa element
Object& Tree<Object>::usun()throw(TreeEmptyException){
  int kierunek=0;
  wezelDrzewa<Object> *tmp=NULL;
  wezelDrzewa<Object> *tmp_back=NULL;
  galaz=0;
  while(root!=NULL){  // gdy istnieje tylko root
    if(allIsNull(root)){
      zawartosc=root->getZawartosc();
      delete(root);
      dl--;
      root=NULL;
      if(dl)throw TreeEmptyException();  // gdy licznik sie nie zgadza z iloscia elementow
      return zawartosc;
    }
    tmp=root;
    while(!allIsNull(tmp)){  // gdy sa jacys synowie
      tmp_back=tmp;
      tmp=tmp->getSon(IsNull(tmp_back));  // przeskakuje na syna najbardziej po prawej
      kierunek=IsNull(tmp_back);          // zpamietuje na ktorego syna przeskoczylo
    }
    zawartosc=tmp->getZawartosc();
    delete(tmp);                          // zwolnienie pamieci
    tmp_back->setSon(NULL,kierunek);      // czyszczenie syna
    dl--;
    if(dl)return zawartosc;
  }
  throw TreeEmptyException();
}

template <class Object>                   // drzewo binarne ktore dziedzicy po ogolnym
class BinaryTree:public Tree<Object>{
public:
  Object& dodaj(const Object& obj);
};

template <class Object>  // dodaje element wiekszy lub rowny na prawej galezi a mniejszy na lewej. mechanizmy podobne jak w funkcji zaslonietej
Object& BinaryTree<Object>::dodaj(const Object& obj){
  wezelDrzewa<Object> *wezel=NULL;
  wezelDrzewa<Object> *tmp_back=NULL;
  wezelDrzewa<Object> *tmp=this->root;
  wezel=new wezelDrzewa<Object>;
  wezel->setZawartosc(obj);
  this->zawartosc=wezel->getZawartosc();
  (this->dl)++;
  while(tmp!=NULL){
    tmp_back=tmp;
    if((wezel->getZawartosc())<(tmp->getZawartosc())){
      tmp=tmp->getSon(0);
    }else{
      tmp=tmp->getSon(1);
    }
  }
  wezel->setParent(tmp_back);
  if(tmp_back==NULL){
    this->root=wezel;
  }else{
    if(wezel->getZawartosc()<tmp_back->getZawartosc()){
      tmp_back->getSon(0)=wezel;
    }else{
      tmp_back->getSon(1)=wezel;
    }
  }
  return this->zawartosc;
}
