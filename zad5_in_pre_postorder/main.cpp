#include<iostream>
#include <string>
#include "Tree.hh"
#include "Tree_ogolne.hh"

using namespace std;

int dl_kolejki=0;

int main(int argc, char *argv[]){
  tree<int> drzewo;
  BinaryTree<int> drzewo_binarne;
  for(int i=0;i<20;i++)drzewo.dodaj(i%10);  
  for(int i=0;i<20;i++)drzewo_binarne.dodaj(i%10);
  
  cout<<endl<<"Zawartosc korzenia drzewa ogolnego: "<<drzewo.getRoot()<<endl;
  cout<<"Zawartosc korzenia drzewa binarnego: "<<drzewo_binarne.getRoot()<<endl;
  cout<<endl<<"Przejscia pre pos i inorder dla drzewa ogolnego: "<<endl<<endl;
  drzewo.preorder();
  drzewo.postorder();
  drzewo.inorder();

  cout<<endl<<"Przejscia pre pos i inorder dla drzewa binarnego: "<<endl<<endl;  
  drzewo_binarne.preorder();
  drzewo_binarne.postorder();
  drzewo_binarne.inorder();

  cout<<endl<<"Wyskokosc drzewa zwyklego: "<<drzewo.wysokosc()<<endl;  
  cout<<"Wyskokosc drzewa binarnego: "<<drzewo_binarne.wysokosc()<<endl<<endl;  

  try{cout<<"Usunieto: "<<drzewo.usun()<<endl;}
  catch(treeEmptyException){cout<<"Drzewo ogolne jest Puste"<<endl;}
  try{cout<<"Usunieto: "<<drzewo_binarne.usun()<<endl;}
  catch(TreeEmptyException){cout<<"Drzewo binarne jest Puste"<<endl;}

  if(drzewo.clear())cout<<endl<<"Drzewo ogolne zostalo wyczyszczone"<<endl;
  if(drzewo_binarne.clear())cout<<"Drzewo binarne zostalo wyczyszczone"<<endl<<endl;

  try{cout<<"Usunieto: "<<drzewo.usun()<<endl;}
  catch(treeEmptyException){cout<<"Drzewo ogolne jest Puste"<<endl;}
  try{cout<<"Usunieto: "<<drzewo_binarne.usun()<<endl;}
  catch(TreeEmptyException){cout<<"Drzewo binarne jest Puste"<<endl;}

  //  cout<<"Is empty: "<<drzewo.isEmpty()<<endl;
  //  cout<<"Is empty: "<<drzewo_binarne.isEmpty()<<endl;
  if(!drzewo.clear())cout<<"Nie zwolniono calej pamieci drzewa"<<endl;
  if(!drzewo_binarne.clear())cout<<"Nie zwolniono calej pamieci drzewa binarnego"<<endl;
  cout<<endl;
  return 0;
}
