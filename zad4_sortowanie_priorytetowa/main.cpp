#include<iostream>
#include <string>
#include "kolejka.hh"

using namespace std;

int dl_kolejki=0;
bool sort(int *dane,int rozmiar);

int main(int argc, char *argv[]){
  int wartosci[15]={5,8,2,6,2,8,24,53,123,831,1234,1,0,-1,6};  // elementy do sortowania
  int rozmiar=sizeof(wartosci)/sizeof(int);
  cout<<endl;
  cout<<"Tablica do posortowania: "<<endl;
  for(int i=0;i<rozmiar;i++)cout<<wartosci[i]<<" ";  // dodawanie elementow do kolejki z priorytetem rownym wartosci  
  sort(wartosci,rozmiar);
  cout<<endl<<"Posortowana tablica: "<<endl;
  for(int i=0;i<rozmiar;i++)cout<<wartosci[i]<<" ";  // dodawanie elementow do kolejki z priorytetem rownym wartosci  
  cout<<endl<<endl;
  return 0;
}

bool sort(int *dane,int rozmiar){
  KolejkaPrio<int> kolejka;                        // kolejka priorytetowa
  for(int i=0;i<rozmiar;i++)kolejka.dodaj(dane[i],dane[i]);  // dodawanie elementow do kolejki z priorytetem rownym wartosci  
  for(int i=0;i<rozmiar;i++){
    try{
      dane[i]=kolejka.usunMin();
    }catch(KolejkaPrioEmptyException){
      kolejka.clear();
      return 0;
    }
  }
  kolejka.clear();
  return 1;
}
