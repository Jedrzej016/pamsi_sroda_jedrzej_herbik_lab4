
class doubleListEmptyException{};
extern int dl_kolejki; // zmienna do okreslania przecieku pamieci
template <class Object> // szablon kolejki
class doubleListInside{
private:
  Object zawartosc;    // dane
  doubleListInside<Object> *next; // wskaznik do przodu
  doubleListInside<Object> *back; // wskaznik do tylu
public:
  doubleListInside();
  ~doubleListInside();
  Object& getZawartosc(){return zawartosc;}
  Object& setZawartosc(Object dane){zawartosc=dane;return zawartosc;}
  doubleListInside<Object>*& getNext(){return next;}
  doubleListInside<Object>*& setNext(doubleListInside<Object> (*dane)){next=dane;return next;}
  doubleListInside<Object>*& getBack(){return back;}
  doubleListInside<Object>*& setBack(doubleListInside<Object> (*dane)){back=dane;return back;}
};

template <class Object>
doubleListInside<Object>::doubleListInside(){
  next=NULL;
  back=NULL;
  // zawartosc=NULL;
  dl_kolejki++;
}

template <class Object>
doubleListInside<Object>::~doubleListInside(){dl_kolejki--;}

template <class Object>
struct doubleList{
  doubleListInside<Object> *next; // wskaznik do przodu
  doubleListInside<Object> *back; // wskaznik do tylu
  int dl;              // dlugosc kolejki
  Object zwrot;    // dane
public:
  doubleList();
  //  ~doubleList();
  int rozmiar() const;
  bool isEmpty() const; 
  bool clear();         // wyczysc
  bool screen();
  Object& pierwszy() throw(doubleListEmptyException);
  Object& ostatni() throw(doubleListEmptyException);
  Object& wstawPierwszy(const Object& obj);
  Object& usunPierwszy() throw(doubleListEmptyException);
  Object& wstawOstatni(const Object& obj);
  Object& usunOstatni() throw(doubleListEmptyException);
  Object& getZawartosc(){return zwrot;}
  Object& setZawartosc(Object dane){zwrot=dane;return zwrot;}
  doubleListInside<Object>*& getNext(){return next;}
  doubleListInside<Object>*& setNext(doubleListInside<Object> (*dane)){next=dane;return next;}
  doubleListInside<Object>*& getBack(){return back;}
  doubleListInside<Object>*& setBack(doubleListInside<Object> (*dane)){back=dane;return back;}
};

template <class Object>
bool doubleList<Object>::screen(){
  doubleListInside<Object> *tmp=NULL;
  tmp=next;
  //  std::cout<<std::endl<<"next: "<<next<<std::endl<<"back: "<<back<<std::endl<<"tmp: "<<tmp<<std::endl;
  if(next==NULL){
    std::cout<<"Kolejka jest pusta";
    return false;
  }
  while(next!=NULL){
    std::cout<<(*next);    // wyswietlanie
    next=next->getNext();  // przewijanie listy
  }
  next=tmp;
  //  std::cout<<std::endl<<"next: "<<next<<std::endl<<"back: "<<back<<std::endl<<"tmp: "<<tmp<<std::endl;
  if((next!=NULL)&&(back!=NULL)&&(next->getBack()==NULL)&&(back->getNext()==NULL)){
    return true;  // gdy powodzenie
  }
  return false;
}

template <class Object>
doubleList<Object>::doubleList(){
  next=NULL;
  back=NULL;
  //  dl_kolejki++;
  dl=0;
}

template <class Object>
int doubleList<Object>::rozmiar() const{
  return dl;
}

template <class Object>
bool doubleList<Object>::isEmpty() const{
  if(dl) return false;
  return true;
}

template <class Object>
bool doubleList<Object>::clear(){
  doubleListInside<Object> *tmp=NULL;
  while(next!=NULL){ // dopoki sa elementy
    tmp=next->getNext();  // przejscie na nastepne elementy
    //    cout<<"Usunieto znak: "<<next->zawartosc<<endl; // kontrola dzialania funkcji
    delete(next); // zaolnienie pamieci
    next=tmp; // przesuniecie korzenia
    dl=0;
  }
  back=NULL; // wyczyszczenie wskaznika
  //  cout<<"Kolejka jest pusta"<<endl; // kontrola dzialania funkcji
  if((next==NULL)&&(back==NULL))return true;
  return false;
}

template <class Object>
Object& doubleList<Object>::pierwszy() throw(doubleListEmptyException){
  if(next!=NULL){
    return next->getZawartosc(); // zwrocenie wartosci pierwszego elementu
  }else{
    //cout<<"Kolejka jest pusta"<<endl;
    throw doubleListEmptyException(); // zrzucenie bledu
  }
}

template <class Object>
Object& doubleList<Object>::ostatni() throw(doubleListEmptyException){
  if(next!=NULL){
    return back->getZawartosc(); // zwrocenie wartosci ostatniego elementu
  }else{
    //cout<<"Kolejka jest pusta"<<endl;
    throw doubleListEmptyException(); // zrzucenie bledu
  }
}

template <class Object>
Object& doubleList<Object>::wstawPierwszy(const Object& obj){
  doubleListInside<Object> *tmp=NULL;
  if(next!=NULL){ // dostawienie komorki na poczatku
    tmp=next;
    next=new doubleListInside<Object>;
    next->getZawartosc()=obj;
    dl++;    
    next->getNext()=tmp;
    tmp->getBack()=next;
  }else{ // wstawenie na wskazniku next gdy kolejka pusta
    tmp=new doubleListInside<Object>;
    tmp->getZawartosc()=obj;
    dl++;    
    next=tmp; // przypisanie do wskaznika poczatku
    back=tmp; // przypisanie do wskaznika konca
  }  
  return next->zawartosc;
}

template <class Object>
Object& doubleList<Object>::usunPierwszy() throw(doubleListEmptyException){
  doubleListInside<Object> *tmp=NULL;
  if(next!=NULL){ // gdy kolejka zawiera elementy
    tmp=next->getNext();
    //    cout<<"Usunieto znak: "<<next->zawartosc<<endl;
    zwrot=next->getZawartosc();
    if(tmp!=NULL){
      tmp->getBack()=NULL;
    }
    delete(next); // zwolnienie pamieci
    next=tmp;
    if(next==NULL)back=NULL;
    dl--;
    return zwrot;
  }else{ // gdy kolejka pusta
    //cout<<"Kolejka jest pusta"<<endl;
    throw doubleListEmptyException(); // zrzucenie bledu 
  }
}

template <class Object>
Object& doubleList<Object>::wstawOstatni(const Object& obj){
  doubleListInside<Object> *tmp=new doubleListInside<Object>;
  tmp->setZawartosc(obj);
  if(next==NULL){ // gdy kolejka pusta
    next=tmp;
  }else{          // gdy kolejka zawiera elementy
    back->getNext()=tmp;
    tmp->getBack()=back;
  }
  back=tmp; // wprzypisanie pamieci do wskaznika koncowego
  dl++;
  return back->getZawartosc();
}

template <class Object>
Object& doubleList<Object>::usunOstatni() throw(doubleListEmptyException){
  doubleListInside<Object> *tmp=NULL;
  if(next!=NULL){ // gdy w kolejce sa elementy
    //    cout<<"Usunieto znak: "<<back->zawartosc<<endl;
    zwrot=back->getZawartosc();
    tmp=back->getBack();
    dl--;
    delete(back); // zwolnienie pamieci 
    if((tmp)!=NULL)tmp->getNext()=NULL; // zerowanie wskaznika elementu krancowego
    back=tmp;
    if(back==NULL)next=NULL; // zerowanie wskaznikow gdy sunieto ostatni element
    return zwrot;
  }else{ // gdy kolejka pusta
    //cout<<"Kolejka jest pusta"<<endl;
    throw doubleListEmptyException(); // zrzucenie bledu
  }
}


template <class Object>
std::ostream& operator << (std::ostream& StrmWy,const doubleList<Object> & WyswietlanaWartosc){
  doubleListInside<Object> *tmp=NULL;
  tmp=WyswietlanaWartosc.getNext();
  //  std::cout<<std::endl<<"next: "<<next<<std::endl<<"back: "<<back<<std::endl<<"tmp: "<<tmp<<std::endl;
  if(tmp==NULL){
    std::cout<<"Kolejka jest pusta";
    return StrmWy;
  }
  while(tmp!=NULL){
    std::cout<<(*tmp);
    tmp=tmp->getNext();
  }
  return StrmWy;
}

template <class Object>
std::ostream& operator << (std::ostream& StrmWy,doubleListInside<Object> &WyswietlanaWartosc){
  std::cout<<WyswietlanaWartosc.getZawartosc()<<" ";
  return StrmWy;  
}

template <class Object>
std::istream& operator >> (std::istream& StrmWy,doubleList<Object> &dane){
  Object wartosc;
  std::cin>>wartosc;  
  dane.wstawOstatni(wartosc);
  return StrmWy;  
}
