#include<cstdlib>
#include<ctime>
#include "lista.hh"

class treeEmptyException{};
extern int dl_kolejki; // zmienna do okreslania przecieku pamieci

template <class Object>
class treewezel{
protected:
  Object zawartosc;    // dane
  treewezel<Object> *rodzic; // wskaznik do przodka
  List<treewezel<Object>* > syn; // lista wskaznikiow na galezie
public:
  treewezel(){rodzic=NULL;}
  ~treewezel(){syn.clear();}//std::cout<<"Dl syna: "<<syn.rozmiar()<<" "<<syn.clear()<<"Dl syna: "<<" "<<syn.rozmiar()<<std::endl;}
  int sonSize(){return syn.rozmiar();}
  Object& getZawartosc(){return zawartosc;}
  Object& setZawartosc(const Object &dane){zawartosc=dane;return zawartosc;}
  treewezel<Object>*& getSon(int i);
  treewezel<Object>*& setSon(treewezel<Object> (*dane),int i){syn[i]=dane;return syn[i];}
  treewezel<Object>*& getParent(){return rodzic;}
  treewezel<Object>*& setParent(treewezel<Object> (*dane)){rodzic=dane;return rodzic;}
};


template <class Object>                                // gdy odwolanie jest do nieistniejacego elementu to jest on tworzony i dostawiany po czym zwracany
treewezel<Object>*& treewezel<Object>::getSon(int i){
  if(i>=syn.rozmiar()){
    syn.wstaw(NULL);
    return syn[syn.rozmiar()-1];
  }else return syn[i];
}

template <class Object>
class tree{
protected:
  Object zawartosc;    // dane
  treewezel<Object> *root; // wskaznik do przodu  
  int dl;
  int wsp;
  int pokolenie;
  int galaz;
  void preorder(treewezel<Object> *wezel);
  void postorder(treewezel<Object> *wezel);
  void inorder(treewezel<Object> *wezel);
  void clear(treewezel<Object> *wezel);
  int max_galaz(int liczba);
  treewezel<Object>* przejdz(treewezel<Object> *wezel,int wartosc);
  bool allIsNull(treewezel<Object> *wezel);
  int IsNull(treewezel<Object> *wezel);
public:
  tree();
  ~tree(void){clear(root);}
  int rozmiar() const;
  bool isEmpty() const; 
  bool clear();         // wyczysc
  Object& getRoot()throw(treeEmptyException);
  Object& dodaj(const Object& obj);
  Object& usun()throw(treeEmptyException);
  int& wysokosc();
  int& getWsp(){return wsp;}
  void preorder();
  void postorder();
  void inorder();
};

template <class Object>
tree<Object>::tree(){
  root=NULL;
  dl=0;
  pokolenie=0;
  galaz=0;
  wsp=1;
}

template <class Object>
Object& tree<Object>::getRoot()throw(treeEmptyException){  // zwraca zawartosc korzenia
  if(root!=NULL)return root->getZawartosc();
  throw treeEmptyException();
}

template <class Object>
void tree<Object>::preorder(){  // wywolanie preorder
  std::cout<<"Przejscie Preorder: "<<std::endl;;
  preorder(root);
  std::cout<<std::endl;
}

template <class Object>  // preorder do rekurencji
void tree<Object>::preorder(treewezel<Object> *wezel){
  if(wezel!=NULL){
    std::cout<<wezel->getZawartosc()<<" ";
    for(int i=0;i<wezel->sonSize();i++)preorder(wezel->getSon(i));
  }
}

template <class Object>  // wywolanie postorder
void tree<Object>::postorder(){
  std::cout<<"Przejscie Postorder: "<<std::endl;;
  postorder(root);
  std::cout<<std::endl;
}

template <class Object>  // postorder do rekurencji
void tree<Object>::postorder(treewezel<Object> *wezel){
  if(wezel!=NULL){
    for(int i=0;i<wezel->sonSize();i++)postorder(wezel->getSon(i));
    std::cout<<wezel->getZawartosc()<<" ";
  }
}

template <class Object>
void tree<Object>::inorder(){  // wywolanie inorder
  std::cout<<"Przejscie Inorder: "<<std::endl;;
  inorder(root);
  std::cout<<std::endl;
}

template <class Object>  // rekurencja inorder
void tree<Object>::inorder(treewezel<Object> *wezel){
  bool tmp=true;
  if(wezel!=NULL){
    for(int i=0;i<wezel->sonSize();i++){
      inorder(wezel->getSon(i));
      if(i==0)std::cout<<wezel->getZawartosc()<<" ";
      tmp=false;
    }
    if(tmp)std::cout<<wezel->getZawartosc()<<" ";
  }
}

template <class Object>  // przypisuje wartosc jezeli jest wieksza od aktualnej
int tree<Object>::max_galaz(int liczba){
  if(liczba>galaz)galaz=liczba;
  return liczba;
}

template <class Object>  // zwraca wartosc galaz czyli wysokosc drzewa
int& tree<Object>::wysokosc(){
  galaz=0;
  przejdz(root,-1);// -1 jezeli mierzymy ilosc polaczen, 0 jezeli ilosc wezlow
  return galaz;
}

template <class Object>  // przechodzi drzewo i szuka jego wysokosci
treewezel<Object>* tree<Object>::przejdz(treewezel<Object> *wezel,int wartosc){
  if(wezel!=NULL){
    wartosc++;
    for(int i=0;i<wezel->sonSize();i++)przejdz(wezel->getSon(i),max_galaz(wartosc));
  }
  return wezel;
}

template <class Object>  // zwraca ilosc elemetow
int tree<Object>::rozmiar() const{
  return dl;
}

template <class Object>
bool tree<Object>::isEmpty() const{
  if(dl||root) return false;
  return true;
}

template <class Object>  // wywolanie czyszczenia
bool tree<Object>::clear(){
  clear(root);
  galaz=0;
  if((root!=NULL)||dl)return false;
  return true;
}

template <class Object>  // rekurencyjne wywolanie czyszczenia
void tree<Object>::clear(treewezel<Object> *wezel){
  if(wezel!=NULL){
    for(int i=0;i<wezel->sonSize();i++)clear(wezel->getSon(i));
    delete wezel;
    if((wezel==root)&&(dl==1))root=NULL;
    dl--;
  }
}

template <class Object>  // dodaje element losowo na galaz, jezeli losowy wybor wskaze na galaz nieistniejaca to ja tworzy
Object& tree<Object>::dodaj(const Object& obj){
  treewezel<Object> *wezel=NULL;
  treewezel<Object> *tmp_back=NULL;
  treewezel<Object> *tmp=root;
  int wartosc=0;
  dl++;
  wezel=new treewezel<Object>;                  // alokacja komorki
  wezel->setZawartosc(obj);
  zawartosc=wezel->getZawartosc();
  while(tmp!=NULL){                             // Przechodzi liste az napotka wolne miejsce
    tmp_back=tmp;
    wartosc=std::rand()%(wsp+tmp->sonSize());   // losowe wybieranie
    tmp=tmp->getSon(wartosc);
  }
  wezel->setParent(tmp_back);                   // ustawienie ojca
  if(tmp_back==NULL){
    root=wezel;
  }else{
    tmp_back->getSon(wartosc)=wezel;            // ustawienie syna w ojcu
  }
  return zawartosc;
}

template <class Object>
bool tree<Object>::allIsNull(treewezel<Object> *wezel){  // prawda je�eli wszystcy synowie s� NULL
  if(wezel!=NULL)for(int i=0;i<wezel->sonSize();i++)if(wezel->getSon(i)!=NULL)return 0;
  return 1;
}

template <class Object>  // zwraca liczbe syna ktory jest NULL liczac od lewej
int tree<Object>::IsNull(treewezel<Object> *wezel){
  int zwrot=0;
  if(wezel!=NULL)for(int i=0;i<wezel->sonSize();i++)if(wezel->getSon(i)!=NULL)zwrot=i;
  return zwrot;
}

template <class Object>  // usuwa element najbardziej po prawej
Object& tree<Object>::usun()throw(treeEmptyException){
  int kierunek=0;
  treewezel<Object> *tmp=NULL;
  treewezel<Object> *tmp_back=NULL;
  galaz=0;
  while(root!=NULL){  // usuwa korzen jezeli nie ma synow
    if(allIsNull(root)){
      zawartosc=root->getZawartosc();
      delete(root);  // zwolnienie pamieci
      dl--;
      root=NULL;
      if(dl)throw treeEmptyException();  // jezeli dl sie nie zgadza to wyrzuca blad
      return zawartosc;
    }
    tmp=root;
    while(!allIsNull(tmp)){  // gdy jest jakis syn
      tmp_back=tmp;                       
      tmp=tmp->getSon(IsNull(tmp_back));  // przejdz to syna najbardziej po prawej az jego synowie beda NULL
      kierunek=IsNull(tmp_back);          // zapamietanie do ktorego syna przeszismy
    }
    zawartosc=tmp->getZawartosc();
    delete(tmp);                          // zwolnienie pamieci
    tmp_back->setSon(NULL,kierunek);      // ustawienie wyczyszczonejgo syna na NULL
    dl--;
    if(dl)return zawartosc;
  }
  throw treeEmptyException();
}
